FROM alpine:3.10

RUN apk add --update nodejs npm make python gcc g++ curl && npm install -g gulp-cli

RUN curl -LO https://github.com/googlecodelabs/tools/archive/refs/tags/v2.2.4.tar.gz && tar -xzf v2.2.4.tar.gz && mv tools-2.2.4/site /site && rm -rf  tools-2.2.4 v2.2.4.tar.gz
RUN curl -Lo claat https://github.com/googlecodelabs/tools/releases/download/v2.2.4/claat-linux-amd64 && chmod u+x claat && mv claat /usr/bin/claat
WORKDIR /site
RUN npm install && npm install gulp

